package de.jensd.sweethome.json;

import de.jensd.sweethome.receiver.json.JsonDeviceCommandProcessor;
import de.jensd.sweethome.core.DeviceCommand;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.json.Json;
import javax.json.JsonObject;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jens Deters
 */
public class JsonTest {

    @Test
    public void testLoadDemoJson() {
        String json = loadDemoJson();

        System.out.println("Loaded:\n" + json);

        assertNotNull(json);
    }

    @Test
    public void testParseJson() {
        String json = loadDemoJson();
        StringReader reader = new StringReader(json);
        JsonObject jsonObject = Json.createReader(reader).readObject();
        List<DeviceCommand> deviceCommands = JsonDeviceCommandProcessor.parse(jsonObject);
        deviceCommands.stream().forEach(d -> {
            System.out.println(d.getDevice().get().toString());
            System.out.println(d.getCommand().get());
        });
        assertTrue(deviceCommands.size() == 3);
    }

    public String loadDemoJson() {
        String json = "";
        try {
            json = new String(Files.readAllBytes(Paths.get(getClass()
                    .getResource("/de/jensd/sweethome/json/demo.json").toURI())));
        } catch (IOException | URISyntaxException ex) {
            fail(ex.getMessage());
        }
        return json;
    }
}
