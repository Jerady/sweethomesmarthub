/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.sweethome.receiver.json;

import de.jensd.sweethome.core.Device;
import de.jensd.sweethome.core.DeviceCommand;
import de.jensd.sweethome.core.Send;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author Jens Deters
 */
public class JsonDeviceCommandProcessor {

    public static List<DeviceCommand> parse(JsonObject jsonObject) {
        List<DeviceCommand> commands = new ArrayList<>();
        if (jsonObject.containsKey("devices")) {
            JsonArray jsonArray = jsonObject.getJsonArray("devices");
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject deviceCommandObject = jsonArray.getJsonObject(i);
                Device device = new Device();
                Send.Command sendCommand = Send.Command.TURN_OFF;
                if (deviceCommandObject.containsKey("device")) {
                    JsonObject deviceObject = deviceCommandObject.getJsonObject("device");
                    device.setName(deviceObject.getString("name"));
                    device.setHouseCode(deviceObject.getString("houseCode"));
                    device.setGroupId(deviceObject.getString("groupId"));
                    device.setDeviceId(deviceObject.getString("deviceId"));
                }
                if (deviceCommandObject.containsKey("command")) {
                    String commandString = deviceCommandObject.getString("command");
                    switch (commandString) {
                        case "ON":
                            sendCommand = Send.Command.TURN_ON;
                            break;
                        case "OFF":
                            sendCommand = Send.Command.TURN_OFF;
                            break;
                    }
                }
                commands.add(new DeviceCommand(device, sendCommand));
            }
        }
        return commands;
    }
}
